//
//  TimerInfoModal.swift
//  TimerTest
//
//  Created by Sarath R on 18/05/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit

struct TimerInformation: Codable {
    
    var startTime: TimeInterval?
    var counterSeconds: Int?
    var status: TimerStatus?
}

