//
//  ViewController.swift
//  TimerTest
//
//  Created by Sarath R on 18/05/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    
    // MARK: Connection Objects
    @IBOutlet weak var minutesFirstDigitLabel: UILabel!
    @IBOutlet weak var minutesSecondDigitLabel: UILabel!
    @IBOutlet weak var secondsFirstDigitLabel: UILabel!
    @IBOutlet weak var secondsSecondDigitLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    
    
    // Declarations
    var timer: Timer!
    var startTime: TimeInterval!
    var counterSeconds = 0
    
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerObservers()
        awakeFromSleep()
    }
    
    
    deinit {
        
        removeObservers()
    }
    
    
    // MARK: Arrange View
    // Register BroadCast Listeners
    func registerObservers() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(TimerViewController.awakeFromSleep), name: .kApplicationWillEnterForeground, object: nil)
    }
    
    
    // Remove all the Broadcast Listeners
    func removeObservers() {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // Rest Display
    func resetValues() {
        
        minutesFirstDigitLabel.text = "0"
        minutesSecondDigitLabel.text = "0"
        secondsFirstDigitLabel.text = "0"
        secondsSecondDigitLabel.text = "0"
        counterSeconds = 0
        startTime = nil
    }
    
    // Reset Timer Engine
    func resetTimer() {
        
        // Validation
        guard timer != nil  else {
            return
        }
        
        // Reset
        timer.invalidate()
        timer = nil
    }
}





// Events
extension TimerViewController {
    
    @IBAction func startAction(_ sender: UIButton) {
        
        configureTimer(for: .start)
    }
    
    @IBAction func pauseAction(_ sender: UIButton) {
        
        configureTimer(for: .pause)
    }
    
    @IBAction func stopAction(_ sender: UIButton) {
        
        configureTimer(for: .stop)
    }
    
    // Set Timer respect with status
    func configureTimer(for status: TimerStatus) {
        
        switch status {
            
        case .start:
            // validation
            guard timer == nil else { return }
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TimerViewController.runTimer), userInfo: nil, repeats: true)
            
            // If start Time not nil then getting back from Pause.
            if startTime == nil {
                startTime = Date.timeIntervalSinceReferenceDate
            }
            
            // Save Time Info
            saveTimer(startTime, counterSeconds: nil, status: .start)
            setSelection(for: .start)
           
        case .pause:
            guard timer != nil else { return }
            resetTimer()
            saveTimer(startTime, counterSeconds: counterSeconds, status: .pause)
            setSelection(for: .pause)
            
        case .stop:
            resetValues()
            resetTimer()
            saveTimer(nil, counterSeconds: nil, status: .stop)
            setSelection(for: .stop)
        }
    }
    
    
    @objc func runTimer() {
        
        counterSeconds += 1
        updateDisplay(counterSeconds)
    }
    
    
    func updateDisplay(_ seconds: Int) {
        
        // Parse & Set Time
        let time = parseMinutesAndSeconds(from: TimeInterval(seconds))
        minutesFirstDigitLabel.text = time.minutes.firstDigit
        minutesSecondDigitLabel.text = time.minutes.secondDigit
        secondsFirstDigitLabel.text = time.seconds.firstDigit
        secondsSecondDigitLabel.text = time.seconds.secondDigit
    }
    
    
    // Parse Time
    func parseMinutesAndSeconds(from time: TimeInterval) -> (minutes: (firstDigit: String, secondDigit: String), seconds: (firstDigit: String, secondDigit: String)) {
        
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        let _minutes = (String(format:"%01i", (minutes / 10)), String(format:"%01i", (minutes % 60 % 10)))
        let _seconds = (String(format:"%01i", (seconds / 10)), String(format:"%01i", (seconds % 60 % 10)))
        return (_minutes, _seconds)
    }
    
    
    // Save Timer Details to Persitant Store
    func saveTimer(_ startTime: TimeInterval?, counterSeconds: Int?, status: TimerStatus) {
        
        // Prepare Data
        var timeInfo = Dictionary<String, Any>()
        if let _startTime = startTime {
            timeInfo["startTime"] = _startTime
        }
        if let _counterSeconds = counterSeconds {
            timeInfo["counterSeconds"] = _counterSeconds
        }
        timeInfo["status"] = status.rawValue
        guard let data = try? JSONSerialization.data(withJSONObject: timeInfo, options: JSONSerialization.WritingOptions.prettyPrinted) else {
            return
        }
        
        // Save on Persitant stoarge
        UserDefaults.save(data, forKey: UserDefaultsKeys.timerInformation.key)
        UserDefaults.synchronize()
    }
    
    
    // Fetch and Parse Saved Timer information
    func getTimerInformation() -> TimerInformation? {
        
        // Fetch data
        guard let data = UserDefaults.getvalue(forKey: UserDefaultsKeys.timerInformation.key) as? Data else {
            return nil
        }
        
        // Parse
        let timerInformation = try? JSONDecoder().decode(TimerInformation.self, from: data)
        return timerInformation
    }
    
    
    
    // Set Selection Style
    func setSelection(for status: TimerStatus) {
        
        switch status {
            
        case .start:
            playButton.backgroundColor = .clear
            pauseButton.backgroundColor = #colorLiteral(red: 1, green: 0.603079617, blue: 0.4258260429, alpha: 0.1)
            stopButton.backgroundColor = #colorLiteral(red: 1, green: 0.603079617, blue: 0.4258260429, alpha: 0.1)
            
        case .pause:
            pauseButton.backgroundColor = .clear
            playButton.backgroundColor = #colorLiteral(red: 1, green: 0.603079617, blue: 0.4258260429, alpha: 0.1)
            stopButton.backgroundColor = #colorLiteral(red: 1, green: 0.603079617, blue: 0.4258260429, alpha: 0.1)
            
        case .stop:
            playButton.backgroundColor = #colorLiteral(red: 1, green: 0.603079617, blue: 0.4258260429, alpha: 0.1)
            pauseButton.backgroundColor = .clear
            stopButton.backgroundColor = .clear
        }
    }
}


extension TimerViewController {
    
    /* Update Time If Needed
     * Condition - Check If any active timer going on,
     * Current Time = ((current time - actual start time) - counter seconds)
     */
    @objc func awakeFromSleep() {
        
        guard let timer = getTimerInformation(), let status = timer.status else {
            return
        }
        
        switch status {
            
        case .start:
            // Validation
            guard let startTime = timer.startTime else {
                return
            }
            
            // Estimate elapsed time
            let currentTime = Date.timeIntervalSinceReferenceDate
            let elapsedTime: TimeInterval = (currentTime - startTime)
            counterSeconds += Int(elapsedTime) - counterSeconds
            awakeTimerIfSleeps()
            setSelection(for: .start)
            
        case .pause:
            // Validation
            guard let timerSecondsOnLastPause = timer.counterSeconds else {
                return
            }
            counterSeconds = timerSecondsOnLastPause
            updateDisplay(counterSeconds)
            setSelection(for: .pause)
            
        case.stop:
            setSelection(for: .stop)
            break
        }
        
        
    }
    
    
    /*  Awake Timer
     *  Condition: Awake Timer if Timer engine is not running and Valid session remains.
     *  Situation: When user kills the app when active session going on.
     */
    func awakeTimerIfSleeps() {
        
        // Validate & Restart Timer
        guard timer == nil else {
            return
        }
        configureTimer(for: .start)
    }
}











