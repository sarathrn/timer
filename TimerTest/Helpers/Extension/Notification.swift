//
//  Notification.swift
//  TimerTest
//
//  Created by Sarath R on 18/05/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let kApplicationWillEnterForeground = Notification.Name("ApplicationWillEnterForeground")
}
