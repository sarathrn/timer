//
//  UserDefaults.swift
//  TimerTest
//
//  Created by Sarath R on 18/05/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation

extension UserDefaults {
    
    static var storage: UserDefaults {
        get {
            return UserDefaults.standard
        }
    }
    
    static func getvalue(forKey: String) -> Any? {
        
        return storage.value(forKey: forKey)
    }
    
    static func save(_ value: Any?, forKey: String) {
        
        storage.set(value, forKey: forKey)
    }
    
    static func removeObject(forKey: String) {
        
        storage.removeObject(forKey: forKey)
    }
    
    static func synchronize() {
        
        storage.synchronize()
    }
}
