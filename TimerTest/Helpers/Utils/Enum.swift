//
//  Enum.swift
//  TimerTest
//
//  Created by Sarath R on 18/05/18.
//  Copyright © 2018 Sarath R. All rights reserved.
//

import Foundation
import UIKit

// Support Timer
enum TimerStatus: Int, Codable {
    
    case start, pause, stop
}


// Support Userdefault Storage
enum UserDefaultsKeys: String {
    
    case timerInformation
    var key: String {
        return self.rawValue
    }
}
